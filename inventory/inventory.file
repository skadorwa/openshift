# Create an OSEv3 group that contains the masters, nodes, and etcd groups
[OSEv3:children]
masters
nodes
etcd
glusterfs
new_nodes

# Set variables common for all OSEv3 hosts
[OSEv3:vars]
# SSH user, this user should allow ssh based auth without requiring a password
ansible_ssh_user=root
openshift_disable_check=memory_availability,disk_availability
#openshift_enable_service_catalog=false
#ansible_service_broker_remove=true
ansible_service_broker_install=true
ansible_service_broker_local_registry_whitelist=['.*-apb$']
ansible_service_broker_registry_whitelist=['.*-apb$']


# default selectors for router and registry services
openshift_router_selector='node-role.kubernetes.io/master=true'
openshift_registry_selector='node-role.kubernetes.io/infra=true'
openshift_metrics_install_metrics=false
openshift_metrics_cassandra_storage_type=dynamic
openshift_metrics_cassandra_storage_class_name=glusterfs-storage
openshift_metrics_cassandra_pvc_size=2Gi
openshift_metrics_cassandra_image=docker.io/openshift/origin-metrics-cassandra:v3.10.0-rc.0
openshift_metrics_hawkular_metrics_image=docker.io/openshift/origin-metrics-hawkular-metrics:v3.10.0-rc.0
openshift_metrics_heapster_image=docker.io/openshift/origin-metrics-heapster:v3.10.0-rc.0
openshift_metrics_schema_installer_image=docker.io/alv91/origin-metrics-schema-installer:v3.10
openshift_disable_check=memory_availability,disk_availability


openshift_release=v3.10
#openshift_image_tag=v3.9.41
openshift_master_dynamic_provisioning_enabled=true
dynamic_volumes_check=false

# If ansible_ssh_user is not root, ansible_become must be set to true
ansible_become=true

openshift_deployment_type=origin
#openshift_deployment_type=openshift-enterprise
openshift_master_default_subdomain=openshift.emeraldowa.pl
openshift_master_cluster_public_hostname=master.openshift.emeraldowa.pl

# uncomment the following to enable htpasswd authentication; defaults to DenyAllPasswordIdentityProvider
openshift_master_identity_providers=[{'name': 'htpasswd_auth', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider'}]
openshift_master_htpasswd_file=/home/htpasswd

#openshift_master_identity_providers=[{'name': 'haproxy-ilb-ldap', 'challenge': 'true', 'login': 'true', 'kind': 'LDAPPasswordIdentityProvider', 'attributes': {'id': ['dn'], 'email': ['mail'], 'name': ['cn'], 'preferredUsername': ['uid']}, 'bindDN': 'CN=ldapread,DC=sdm,DC=de', 'bindPassword': 'Athohx0x', 'insecure': 'true', 'url': 'ldap://de-sdmauth1.sdm.de/DC=sdm,DC=de?uid'}]

osn_storage_plugin_deps=['glusterfs']

openshift_storage_glusterfs_namespace=app-storage
openshift_storage_glusterfs_storageclass=true
openshift_storage_glusterfs_storageclass_default=true
openshift_storage_glusterfs_block_deploy=true
openshift_storage_glusterfs_block_host_vol_size=100
openshift_storage_glusterfs_block_storageclass=true
openshift_storage_glusterfs_block_storageclass_default=false

openshift_storage_glusterfs_timeout=600


# Logging deployment
#
# Currently logging deployment is disabled by default, enable it by setting this

# Logging playbook settings - see /usr/share/ansible/openshift-ansible/roles/openshift_logging/README.md
openshift_logging_install_logging=false
openshift_logging_use_ops=true
openshift_logging_master_public_url=https://master.openshift.emeraldowa.pl:8443

# ES
openshift_logging_curator_default_days=3
openshift_logging_curator_run_hour=23
openshift_logging_curator_run_minute=00
openshift_logging_curator_nodeselector={'node-role.kubernetes.io/infra': 'true'}
openshift_logging_kibana_hostname=kibana.openshift.emeraldowa.pl
openshift_logging_kibana_replica_count=0
openshift_logging_kibana_nodeselector={'node-role.kubernetes.io/infra': 'true'}
openshift_logging_es_cluster_size=2
openshift_logging_es_number_of_replicas=1
openshift_logging_es_number_of_shards=1
openshift_logging_es_memory_limit=2Gi
openshift_logging_es_nodeselector={'node-role.kubernetes.io/infra': 'true'}
#openshift_logging_es_pvc_dynamic=true
#openshift_logging_es_pvc_size=20G

# ES-OPS
openshift_logging_curator_ops_default_days=3
openshift_logging_curator_ops_run_hour=23
openshift_logging_curator_ops_run_minute=00
openshift_logging_curator_ops_nodeselector={'node-role.kubernetes.io/infra': 'true'}
openshift_logging_kibana_ops_hostname=kibana-ops.openshift.emeraldowa.pl
openshift_logging_kibana_ops_replica_count=0
openshift_logging_kibana_ops_nodeselector={'node-role.kubernetes.io/infra': 'true'}
openshift_logging_es_ops_cluster_size=2
openshift_logging_es_ops_number_of_replicas=0
openshift_logging_es_ops_number_of_shards=1
openshift_logging_es_ops_memory_limit=2Gi
openshift_logging_es_ops_nodeselector={'node-role.kubernetes.io/infra': 'true'}
#openshift_logging_es_ops_pvc_dynamic=true
#openshift_logging_es_ops_pvc_size=20G

#ES-IMAGES
openshift_logging_fluentd_image=openshift/origin-logging-fluentd:v3.10
openshift_logging_elasticsearch_image=openshift/origin-logging-elasticsearch:v3.10
openshift_logging_curator_image=openshift/origin-logging-curator:v3.10
openshift_logging_kibana_image=openshift/origin-logging-kibana:v3.10
openshift_logging_kibana_proxy_image=openshift/origin-logging-auth-proxy:v3.10


#PROMETHEUS

# Prometheus deployment
openshift_prometheus_node_selector={"infra":"yes"}
openshift_prometheus_image_version=v3.10
openshift_prometheus_proxy_image_version=v3.10
openshift_prometheus_alertmanager_image_version=v3.10
openshift_prometheus_alertbuffer_image_version=v3.10
#openshift_prometheus_namespace=openshift-monitoring
# Currently prometheus deployment is disabled by default, enable it by setting this
openshift_hosted_prometheus_deploy=false
#
# Prometheus storage config
# Option A - NFS Host Group
# An NFS volume will be created with path "nfs_directory/volume_name"
# on the host within the [nfs] host group.  For example, the volume
# path using these options would be "/exports/prometheus"
openshift_prometheus_storage_access_modes=['ReadWriteOnce']
openshift_prometheus_storage_kind=dynamic
openshift_prometheus_storage_volume_name=prometheus
openshift_prometheus_storage_volume_size=3Gi
#openshift_prometheus_storage_labels={'storage': 'prometheus'}
openshift_prometheus_storage_type='pvc'
openshift_prometheus_storage_class=glusterfs-storage
# For prometheus-alertmanager
openshift_prometheus_alertmanager_storage_access_modes=['ReadWriteOnce']
openshift_prometheus_alertmanager_storage_kind=dynamic
openshift_prometheus_alertmanager_storage_volume_name=prometheus-alertmanager
openshift_prometheus_alertmanager_storage_volume_size=2Gi
#openshift_prometheus_alertmanager_storage_labels={'storage': 'prometheus-alertmanager'}
openshift_prometheus_alertmanager_storage_type='pvc'
openshift_prometheus_alertmanager_storage_class=glusterfs-storage
# For prometheus-alertbuffer
openshift_prometheus_alertbuffer_storage_access_modes=['ReadWriteOnce']
openshift_prometheus_alertbuffer_storage_kind=dynamic
openshift_prometheus_alertbuffer_storage_volume_name=prometheus-alertbuffer
openshift_prometheus_alertbuffer_storage_volume_size=2Gi
openshift_prometheus_alertbuffer_storage_type='pvc'
openshift_prometheus_alertbuffer_storage_class=glusterfs-storage

#GRAFANA
openshift_grafana_timeout=400
openshift_grafana_storage_type=pvc
openshift_grafana_pvc_name=grafana
openshift_grafana_node_selector={'node-role.kubernetes.io/infra': 'true'}
openshift_grafana_sc_name=glusterfs-storage
#openshift_grafana_prometheus_namespace=openshift-metrics
#openshift_grafana_prometheus_serviceaccount=prometheus
#Registry Storage

openshift_hosted_registry_storage_kind=glusterfs


# host group for masters
[masters]
masternode01

# host group for etcd
[etcd]
masternode01

# host group for nodes, includes region info
[nodes]
masternode01 openshift_schedulable=true openshift_node_group_name='node-config-master' 
workernode01 openshift_node_group_name='node-config-compute' 
workernode02 openshift_node_group_name='node-config-compute' 
workernode03 openshift_node_group_name='node-config-compute' 
infranode01 openshift_node_group_name='node-config-infra' 
#infranode02 openshift_node_group_name='node-config-infra'
glusternode01 openshift_node_group_name="node-config-compute"
glusternode02 openshift_node_group_name="node-config-compute"
glusternode03 openshift_node_group_name="node-config-compute"


[new_nodes]
infranode02 openshift_node_group_name='node-config-infra'

# Storage nodes
[glusterfs]
glusternode01 glusterfs_ip=10.142.0.2 glusterfs_devices="[ '/dev/sdc', '/dev/sdd' ]"
glusternode02 glusterfs_ip=10.142.0.3 glusterfs_devices="[ '/dev/sdc', '/dev/sdd' ]"
glusternode03 glusterfs_ip=10.142.0.4 glusterfs_devices="[ '/dev/sdc', '/dev/sdd' ]"






###COMMENTS:
#https://docs.openshift.com/container-platform/3.9/install_config/cluster_metrics.html
#https://access.redhat.com/containers/?tab=tags&platform=openshift#/registry.access.redhat.com/openshift3/metrics-hawkular-metrics
#https://github.com/openshift/openshift-ansible/tree/master/roles/openshift_grafana
#https://docs.openshift.com/container-platform/3.9/install_config/aggregate_logging.html

