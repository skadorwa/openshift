#!/bin/bash

perl -pi -e "s/%%ip%%/$(hostname -I)/g" /etc/cassandra/cassandra.yaml
export CLASSPATH=/kubernetes-cassandra.jar

mkdir -p /cassandra_data/data
mkdir -p /cassandra_data/commitlog
mkdir -p /cassandra_data/saved_caches

cassandra -f -R
