# heketi-cli emulator
# To use, source this file: . ./setup-heketi-cli.sh
glusterfs=app-storage

if oc --request-timeout=1 whoami &>/dev/null; then
  HEKETI_CLI_SERVER="http://"$(oc -n $glusterfs get route heketi-storage --template '{{ .spec.host }}')
  HEKETI_CLI_KEY=$(oc -n $glusterfs get secret heketi-storage-admin-secret --template='{{ .data.key }}' | base64 -d)
  HEKETI_CLI_USER=admin
  HEKETI_POD=$(oc -n $glusterfs get pods -l heketi=storage-pod  --template='{{(index .items 0).metadata.name}}')

  echo "# Variables set:"
  echo "HEKETI_CLI_SERVER=$HEKETI_CLI_SERVER"
  echo -n "HEKETI_CLI_KEY="; echo $HEKETI_CLI_KEY | perl -lpe 's/\S/x/g'
  echo "HEKETI_CLI_USER=$HEKETI_CLI_USER"
  echo "HEKETI_POD=$HEKETI_POD"
  echo ""

  # Define function to run heketi commands in the pod
  echo "# Defining function 'heketi-cli'"
  heketi-cli() {
    oc -n $glusterfs exec -it $HEKETI_POD -- heketi-cli -s $HEKETI_CLI_SERVER --secret "$HEKETI_CLI_KEY" --user $HEKETI_CLI_USER $@
  }

  echo "# Now you may run heketi-cli commands such as:"
  echo ''
  echo "heketi-cli volume list"
  echo "heketi-cli volume info fba791c5bae161291f3b9274cff41c1a"
  echo "heketi-cli cluster list"
  echo "heketi-cli cluster info 030ef9e8d931a41e9a1e44eccd172dbc"
  echo "heketi-cli topology info"
  echo ""
else
  echo "Not logged in."
fi

