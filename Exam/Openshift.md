# Openshift



## 1. Zaczynamy



Openshift korzysta wyłącznie z szyfrowanego połączenia - SSL. Po jego instalacji dostęp do platformy odbywa się poprzez adres `https://localhost:8443`.  Pierwsze logowanie do platformy odbywa się poprzez wpisanie _username_: **developer**, a hasło może być losowe.

Od strony konsoli możemy zalogować się w sposób: `oc login -u system:admin` jest to logowanie głównego **administratora** openshifta.

### 1.1. Tworzenie nowego projektu

Dodanie nowego projektu ogranicza się do wykonania komendy `oc new-project <projectname> ` również komenda `oc help` jest bardzo przydatna!

Nazwa projektu musi być podana z małych liter. W innym wypadku otrzymamy poniższy komunikat:

```
The ProjectRequest "Project1" is invalid: metadata.name: Invalid value: "Project1": a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character (e.g. 'my-name',  or '123-abc', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?')
```

Weryfikacja czy dany projekt został utworzony, powinna zostać przeprowadzona poprzez komendę: `oc get project`.

### 1.2. Dodanie użytkownika do projektu

Dodanie użytkownika do projektu to wykonanie dwóch kroków:

* Przełączenie się na docelowy projekt komendą `oc project <projectname>`
* Dodanie praw w tym przypadku administratora `oc policy add-role-to-user admin <username>`

Inne dostępne role to: `view`, `edit` itp.

### 1.3. Dodawanie nowego _serwisu_

 Aby dodać nową aplikację należy wykonać to polecenie: `oc new-app https://github.com/openshift/ruby-hello-world.git#beta4` na platformie Openshift zostanie uruchomiony _deployment_ a następnie _serwis_ tej aplikacji.

Sparwdzenie liczby podów robimy przy użyciu komendy `oc get pods`, a więcej informacji otrzymamy przy użyciu komendy `oc get pods -o wide`. _Pody_ również możemy skalować za pomocą komendy `oc scale --replicas=2 dc ruby-hello-world`. 

Teraz możemy zweryfikować co zawiera się w danych podach, zrobimy to przy użyciu narzędzia _curl_.

* Najpierw wyciągnijmy adres IP: `oc get pods -o wide`
* Używając adresu IP pod-a zobaczmy co ma w środku `curl http://_address_:_port_` 

Przykładowy wynik został pokazany poniżej:

```HTML
<!DOCTYPE html>
<html>
<head>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

  <title>Hello from OpenShift v3!</title>
</head>
<body>
  <div class="page-header" align=center>
    <h1> Welcome to an OpenShift v3 Demo App! </h1>
  </div>

  
  <div class="container" align=center>
    <h3>(It looks like the database isn't running.  This isn't going to be much fun.)</h3>
  </div>
  
</body>

<script type="text/javascript">
function handleSubmit()
      {
        return false;
      }

document.getElementById('get').onclick = function() {
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET",window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/keys/"+document.getElementById("key").value, false );
    xmlHttp.send( null );
    document.getElementById("response").style.display = 'block';
    document.getElementById("response").innerHTML = xmlHttp.responseText;
}

document.getElementById('put').onclick = function() {
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/keys/"+document.getElementById("key").value, false );
    var params = "value="+document.getElementById("value").value;

    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", params.length);
    xmlHttp.setRequestHeader("Connection", "close");

    xmlHttp.send( params );
    document.getElementById("response").style.display = 'block';
    document.getElementById("response").innerHTML = xmlHttp.responseText;
}

document.getElementById('delete').onclick = function() {
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "DELETE", window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/keys/"+document.getElementById("key").value, false );
    xmlHttp.send( null );
    document.getElementById("response").style.display = 'block';
    document.getElementById("response").innerHTML = "Key deleted.";
}

</script>
</html>
```

Aby otrzymać informacje na temat danego serwisu (a nie podu) należy wywołać komendę:

`oc get svc ruby-hello-world`  próba sprawdzenia zawartości serwisu zawsze będzie skutkowała niepowodzeniem.

Można natomiast uzyskać wiele informacji z poziomu konsoli na temat zarówno serwisu jak i pod-a.

* `oc describe svc ruby-hello-world`
* `oc descrive pod nazwa-poda`

Edycja serwisu odbywa się poprzez wprowadzenie komendy `oc edit svc ruby-hello-world`. Możemy np zmienić typ serwisu na **NodePort** i dodać pod _ports_ **nodePort: 30800** jak podano w przykładzie poniżej

```yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    openshift.io/generated-by: OpenShiftNewApp
  creationTimestamp: 2018-07-18T20:13:20Z
  labels:
    app: ruby-hello-world
  name: ruby-hello-world
  namespace: new-project
  resourceVersion: "8552"
  selfLink: /api/v1/namespaces/new-project/services/ruby-hello-world
  uid: 03b86447-8ac7-11e8-b58e-e82aea8ad94b
spec:
  clusterIP: 172.30.118.206
  ports:
  - name: 8080-tcp
    port: 8080
    protocol: TCP
    targetPort: 8080
    nodePort: 30800
  selector:
    app: ruby-hello-world
    deploymentconfig: ruby-hello-world
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```

Po weryfikacji: `oc describe svc ruby-hello-world` możemy sprawdzić działanie tego rozwiązania: `curl http://127.0.0.1:30800`.

> Co dzięki temu osiągneliśmy? Możemy wystawić naszą aplikację bezpośrednio na jakimś porcie np. 30800, co daje nam wiele możliwości weryfikacji działania aplikacji bez jakiegokolwiek tworzenia dodatkowego routingu. Jest to bardzo eleganckie rozwiązanie.

**Ważne**: każdy pod może być zarządzany od środka, jak pojedynczy kontener dockerowy, poprzez komendę: `oc rsh nazwa-poda`.

Usuwanie projektu należy wykonać komendą: `oc delete project nazwa-projektu`.

Podsumowując:

**Openshift** posiada trzy rozwiązania pozwalające dostać się do zawartości pod-a: 

1. **HostPort** - nie jest omawiane w DO280
2. **NodePort** - przykład wykonywany powyżej -  pozwala na wystawienie portu poprzez prostą edycję **oc edit svc** 
3. Openshift **routes** - pozwala na ruch **tylko** HTTP, HTTPS, TLS i WebSocket, a cała konfuguracja przedstawiona została w drugim rozdziale.



## 2. Routes - tworzenie i weryfikacja



​	W przeciwieństwie do **NodePort** gdzie wystawialiśmy port z przedziału **(30000-32767)** tworzenie tak zwanych **routs** wymaga wygenerowania certyfikatu itp. Ponieżej został przedstawiony zestaw kroków które należy wykonać. **Uwaga!** to zadanie jest bardzo istotne. Więcej na jego temat jest opisane na stronie 68 podręcznika DO280.

* Pierwszym krokiem będzie usunięcie naszego poprzedniego projektu i stworzenie nowego.

Wyświetlamy nasze projekty: `oc get projects` i usuwamy projekt któy nas najbardziej interesuje: `oc delete project new-project`.

* Tworzymy nowy projekt o nazwie _secure-route_ i tworzymy nową (tą samą) aplikację.

Aby stworzyć nowy projekt używamy komendy: `oc new-project secure-route` i uruchamiamy aplikacje: `oc new-app https://github.com/openshift/ruby-hello-world.git#beta4`. 

* Za pomocą komendy `oc get pods -o wide` otrzymujemy liste podów z ich adresami IP

```
NAME                       READY     STATUS      RESTARTS   AGE       IP           NODE
ruby-hello-world-1-build   0/1       Completed   0          1m        172.17.0.3   localhost
ruby-hello-world-1-zsdxk   1/1       Running     0          22s       172.17.0.3   localhost

```

* **Bardzo ważny krok**: tworzymy własne certyfikaty SSL.


```bash
openssl genrsa -out test.localhost.key 2048
openssl req -new -key test.localhost.key -out test.localhost.csr -subj "/C=US/ST=NC/L=Raleigh/O=RedHat/OU=RHT/CN=test.localhost"
openssl x509 -req -days 366 -in test.localhost.csr -signkey test.localhost.key -out test.localhost.crt
```

Zweryfikuj czy zostały utworzone trzy pliki i są one umieszczone w jednym folderze, jeśli nie przerzuć je do niego.

* Tworzenie routa do naszego serwisu.

`oc create route edge --service=ruby-hello-world --hostname=test.localhost --key=test.localhost.key --cert=test.localhost.crt`

Aby zweryfikować routy należy wpisać komende: `oc get routes` jej wynik jest poniżej.

```
NAME               HOST/PORT        PATH      SERVICES           PORT       TERMINATION   WILDCARD
ruby-hello-world   test.localhost             ruby-hello-world   8080-tcp   edge          None
```

* Weryfikacja stworzonego routa:

  * `oc get route/ruby-hello-world -o yaml` sprawdź czy zawiera certyfikat
  * `curl http://test.localhost` sprawdź czy otrzymasz błąd o niedostępności aplikacji
  * `curl -k -vvv https://test.localhost` sprawdź czy  aplikacja jest dostępna, wraz z wynikiem działania curl-a

* Używanie polecenia: `oc describe` 

  * `oc describe svc ruby-hello-world`
  * `oc describe pod ruby-hello-world-1-1ad`
  * `oc describe route ruby-hello-world`

  **(do przećwiczenia)**

Również możemy sprawdzić naszą aplikację w przeglądarce WWW wpisując adres `https://test.localhost`. Po zakończeniu sprawdzania usuwamy projekt komendą: `oc delete project secure-route`.



### Podsumowanie

(później + WILDCARDs)



## 3. Komendy w Openshift



> Dziwną rzeczą jest, że jest to dopiero na tym etapie kursu DO280, ale trudno.

W Openshifcie wszystko jest _resourcem_ te które używane były dotychczas to:

* nodes
* services
* pods
* projects
* deploymentConfigs
* users

Dane na ich temat można uzyskać w różny sposób a w tym rozdziale nauczymy się te sposoby wykorzystywać.

Dwie bardzo ważne komendy to: `oc types` oraz `oc help` - Openshift posiada bardzo rozbudowany wewnętrzny instruktaż. 

Ważne komendy które już poznaliśmy to:

* `oc whoami` - informuje nas kim aktualnie jesteśmy zalogowani
* `oc new-project <project-name>` - tworzenie nowego projektu
* `oc status` - informuje nas o recourach na danym projekcie
* `oc delete project <project-name>` - usuwa projekt z platformy
* `oc logout` - pozwala na wylogowanie się z Openshift-a
* `oc login -u system:admin` - logowanie się użytkownikiem o pełnych prawach 

Następną grupą komend to komendy dotyczące wszytskich zasobów:

* `oc get RESOURCE_NAME` - np. `oc get pods` etc. dodając flagę `-o wide` otrzymujemy bardziej szczegółowe informacje na temat zasobów. Flaga `-w` to taki `watch` więc możemy za jej pomocą odświeżać widok np. podów
* `oc get all` - otrzymujemy informacje na temat wszystkich zasobów
* `oc describe RESOURCE_NAME` - więcej informacji na temat zasobu
* `oc export` - pozwala na eksport do pliku konfiguracji np. serwisu czy routu
* `oc create` - po edycji pliku konfiguracyjnego np. z poprzedniej komedny możemy stworzyć taki zasób
* `oc delete RESOURCE_NAME` - usunięcie zasobu - warto tu dodać, że jeśli usuniemy pod-a to ten stworzy się na nowo
* `oc exec` - wykonywanie komend wewnątrz kontenera

**Openshift resource types**:

**Container**

​	Jeden proces uruchomiony wewnątrz środowiska Linux - przenośny i skalowalny

**Image**

​	Obraz konteneru, który może zostać przechowywany w repozytorium np. DockerHub-ie

**Pod**

​	Zestawy jednego lub wielu kontenerów wdrożonych na nod-a, które posiadają unikatowy adres IP oraz wolumeny. Definiują one również ustawienia bezpieczeństwa dla każdego z kontenerów.

**Label**

​	Pary klucz-wartość przypisane do każdego z zasobu aby je pogrupować i wyselekcjonować. Można wtedy łatwiej zarządzać gdzie dany pod czy serwis się wdroży, na jakiego noda.

**Volume**

​	Pliki w kontenerach są efemeryczne więc trzeba je montować opcja **Pesistent Volume** ułatwia taki zabieg

**Node**

​	Workery które są zarządzane przez Masterów

**Service**

​	Logiczna nazwa grupy podów. Posiada on własny adres IP który może zostać wystawiony (poprzez route albo wystawiony port). Posiada również nazwę DNS.

**Route**

​	Wystawienie nazwy DNS dla danego serwisu, który dostępny będzie z poza klastra Openshift.

**Replication** **Controller**

​	Kontroler pilnujące aby liczba podów się zgadzała z ustaloną. Jeśli pod zostanie ubity wtedy stworzony zostanie nowy pod.

**Deployment Configutation**

​	Templatka, która jest analogią do mikroserwisu. Pisana np. w YAML-u pozwala na zaprojektowanie całego systemu i wdrożenie go poprzez uruchomienie pliku. (Jest to plik dc.yml)

**Build Configuration**

​	Opis jak zbudować kod źródłowy do nowego obrazu. Np budowanie Dockerfila do obrazu kontenera

**Build**

​	Budowanie obrazu kontenera

**Image Streams and Image Stream Tags**

​	Tagowanie obrazu kontenera, ostatni wykonany to _:latest_

**Secret**

​	Będzie o tym później - jest to przechowywanie haseł w obrazach tak aby były one bezpieczne

**Project**

​	Projekt posiada listę członków, ich ról jak **view, edit, admin**, ustwienia bepieczeństwa, limity zasobów itp.

### 3.1. Tworzenie Aplikacji

Jest wiele metod utworzenia aplikacji w Openshifcie, najlepiej każdą wcześniej dopasować tworząc plik dc.yml możemy wtedy użyć opcji `oc new-app -o plik.yaml`, poniżej inne dostępne opcje.

`oc new-app mysql MYSQL_USER=user MYSQL_PASSWORD=pass MSQL_DATABASE=testdb -l db=mysql` - tworzenie aplikacji prosto z Dockerhub-a jak i dodatkowe ustawianie jej parametrów

`oc new-app --docker-image=myregistry.com/ala/ma/kota/my-app --name=myapp` - tworzenie aplikacji, której obraz znajduje się w prywatnym repozytorium

`oc new-app https://github.com/openshift/ruby-hello-world --name ruby-hello` - tworzenie aplikacji bazującej na kodzie źródłowym z repozytorium (przykład source to image - S2I)

`oc new-app https://myrepo/php-hello -i php:7.0 --name=php-hello` - tworzenie aplikacji z własnego repozytorium jak i odwołanie się do istniejącego już obrazu np. php:7.0

### 3.2. Wykorzystanie komend + troubleshooting

`oc get events` komenda pokazująca cykl życia eventów.

Aby otrzymać templatke z pod-a należy użyć `oc export pod ruby-ex-1-n59xc --as-template=ruby > ruby.yml`  tak wyeksportowaną templatkę możemy edytować i wdrożyć ponownie.

Z dostępnych komend dotyczących diagnostyki:

* `oc adm diagnostics` 
* `oc get events -n <project>`
* `oc log <pod>` dla poda i `oc log bc/<build-name>` dla builda
* `oc rsh <pod>` wejście do shell-a pod-a
* `oc rsync <pod>:<pod_dir> <local_dir> -c <container>` kopiowanie z pod-a do lokalnej maszyny
* `oc rsync <local_dir> <pod>:<pod_dir> -c <container>` kopiowanie pliku z lokalnej maszyny do pod-a
* `oc port-forward <pod> <local_port>:<remote_port>` przekierowanie portu



### 3.3. Jak ustawić insecure-registry

1. Edytuj plik /etc/sysconfig/docker
2. Wstaw linie: `OPTIONS='--insecure-registry 172.30.0.0/16 --selinux-enabled --log-level=debug'`
3. Docker powinien być zrestartowany





## Nowy rozdział - Minishift

Z powodu pewnych błędów w wersji stand alone Openshifta na Linuxie Fedora, w dalszej części wszystko będzie uruchamiane na Minishifcie.

### 1. Instalacja

Aby zainstalować Minishifta należy wykonać kroki tego [tutorialu](https://fedoramagazine.org/run-openshift-locally-minishift/). Problem pojawia się natomiast z pobieraniem obrazów co zostało wyjaśnione [tu](https://github.com/minishift/minishift/issues/109). Rozwiązanie to dodanie `nameserver 8.8.8.8` do pliku **`/etc/resolv.conf`** 

* minishift ssh

* sudo vi /etc/resolv.conf

* add `nameserver 8.8.8.8`

* `sudo systemctl restart dnsmasq`



> Jeśli używasz minishifta to w routingu użyj takiego adresu `192.168.42.157.nip.io`



## 5. Zarządzanie dostępem użytkowników

Każdy projekt oprócz nazwy czy opisu posiada również:

- Objekty: pod-y, serwisy, kontrolery itp
- Polityki: czyli zasady które są stosowane dla danych projektów
- _Constraints_: wprowadzenie limitów dla zasobów



Administratorowie mogą stworzyć użytkownika i przypisać mu polityki dla danego projektu. Więcej na ten temat oraz typy użytkowników znajdują się w DO280 str. 127.

W Openshifcie znajduje się tak zwany **Cluster Administrator** czyli pewne uprawnienie konta dzięki któremu możma delegować prawa administratorskie projektom jak i użytkownikom. Dzięki temu można przydzielić komuś administratora w obrębie danego projektu.



**Restricting project creation**  - usuwamy możliwość do własnego tworzenia projektów dla grup: **system:authenticated** i **system:authenticated:oauth**

```bash
oc adm policy remove-cluster-role-from-group \
	self-provisioner \
	system:authenticated \
	system:authenticated:oauth
```

Wynik tego działania to pokazany jest na poniższym logu:

```
oc new-project test
Error from server (Forbidden): You may not request a new project via this API.
```

Wykonywane zostało to na koncie developera. Ma to za zadanie uniemożliwić innym osobą niż system:admin, tworzenie nowych projektów.



**Granding project creation** - dodajemy tą możliwość

```bash
oc adm policy add-cluster-role-from-group \
	self-provisioner \
	system:authenticated \
	system:authenticated:oauth
```

Tworzenie projektu ze wszystkimi opcjami:

```bash
oc new-project demoproject \
	--description="Demo" \
	--display-name="demo_project"
```



Aby zobaczyć jakie role są w klastrze należy wykonać komende: `oc get clusterrolebindings` jeśli chodzi natomiast o role projektowe, aby je sprawdzić trzeba wykonać komendę: `oc get rolebindings`. 

### 5.1 Typy użytkowników

* **Regular users** - zwykli użytkownicy, nie mający zbyt wiele praw
* **System users** - wiele kont jest stworzona automatycznie, mogą wykonywać wiele rzeczy wewnatrz platformy. Takim użytkownikiem jest **system:admin**
* **Service accounts** - specjalne uprawnienie użytkownika które przypisane jest do konkretnego projektu. Reprezentowane jest poprzez objekt: **ServiceAccount**.

Rodzaje użytkowników sprawdzane są jedynie w momencie uwierzytelnienia. Autoryzacja natomiast to odrębny dział.

### 5.2 SCCs

Czyli _security context constrains_ - zwiększenie uprawnień dla jakiegoś serwisu / poda.

* Sprawdzamy nazwę: `oc get scc`

* Możemy sprawdzić zawartość: `oc describe scc scc_name` 

* Zwiększyć uprawnienia dla usera lub grupy

  `oc adm policy add-scc-to-user scc_name user_name`

  `oc adm policy add-scc-to-group scc_name group_name`

* Usuwanie takich polity odbywa się przez `remove-scc-from-*`

Normalnie Openshift nie pozwala na uruchomienie kontenera jako root. Dzięki modyfikacja SCCsa można to jednak zrobić. str. 128 DO280

### 5.3 Service Account

Można również stworzyć takie konto i wtedy użyć go w aplikacji zamiast root-a ale z jego uprawnieniami.

1. Tworzenie konta `oc create serviceaccount useroot`

2. Dodanie konta servisowego do dc

   ```
   oc path dc/demo-app
   --patch '{"spec":{"template":{"spec":{"serviceAccountName": "useroot"}}}}'
   ```

3. Dodanie konta do _anyuid_ `oc adm policy add-scc-to-user anyuid -z useroot`



### 5.4 Tworzenie użytkowników

Aby uworzyć użytkownika należy:

1. Wykonać taką komendę: `oc create user demo-user`
2. Dodać mu hasło: `htpasswd /etc/origin/openshift-passwod demo-user`
3. Dodaj role użytkownikowi

**Role użytkownika** - dodaje się je za pomocą komendy **oc policy add-role-to-user**.

`oc policy add-role-to-user edit demo-user` - dodanie opcji edycji

`oc policy add-role-to-user view demo-user` - dodanie opcji przeglądania

usuwanie ról wykonuje sie przy użyciu: `remove-role-from-user`.

Na 132 stronie DO280 został przedstawiony przykład jak to wygląda w praktyce.



### 5.5 Zarządzanie wrażliwymi danymi - Secrets

Są to zmienne, które umożliwiają tworzenie haseł i ich przechowywanie w bezpieczny sposób.

Można stworzyć to za pomocą `oc create secret generic secret_name` zobaczymy to później w Resources -> Secrets. Możemy w ten sposób stworzyć unikalny i nieznany ciąg znaków dla pewnych wartości np.:

```
oc create secret generic secret_name \
  --from-literal=username=root \
  --from-literal=password=root123
```

Monżna następnie dodać to do aplikacji. Działa to mniej więcej jak credentials w Jenkinsie. Dodawanie secretów do aplikacji można również poprzez GUI.

`   oc get secrets secret1name -o yaml` - wyciągnięcie danych do yamla.

### 5.6 ConfigMap

Przechowywanie wartości klucz=wartość, tworzone jak secrety. Używane do przechowywania pewnych powtarzających danych bez potrzeby ich szyfrowania.

```
oc create configmap special-config \
  --from-literal=serverAddress=192.168.2.2
```



### 5.7 Wykorzystanie - ćw praktyczne

1. Logujemy się jako developer

   `oc login -u developer`

2. Tworzymy nowy projekt pod nazwą _secure-secrets_

   `oc new-project secure-secrets`

3. Tworzymy nowe secrets

   ```bash
   oc create secret generic mysql \
    --from-literal='database-user'='mysql' \
    --from-literal='database-password'='redhat' \
    --from-literal='database-name'='testmysql' \
    --from-literal='database-root-password'='do280-admin'
   ```

4. Uruchamiamy aplikacje z pliku podanego poniżej komendą: `oc new-app --file=mysql-lab.yaml`

```yaml
apiVersion: v1
kind: List
items:
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      creationTimestamp: null
      generation: 1
      name: mysql
    spec:
      replicas: 1
      revisionHistoryLimit: 10
      strategy:
        activeDeadlineSeconds: 21600
        resources: {}
        rollingParams:
          intervalSeconds: 1
          maxSurge: 25%
          maxUnavailable: 25%
          timeoutSeconds: 600
          updatePeriodSeconds: 1
        type: Rolling
      template:
        metadata:
          annotations:
            openshift.io/generated-by: OpenShiftNewApp
          creationTimestamp: null
          labels:
            name: mysql
            deploymentconfig: mysql
        spec:
          containers:
          - env:
            - name: MYSQL_USER
              valueFrom:
                secretKeyRef:
                  key: database-user
                  name: mysql
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: database-password
                  name: mysql
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: database-root-password
                  name: mysql
            - name: MYSQL_DATABASE
              valueFrom:
                secretKeyRef:
                  key: database-name
                  name: mysql
            image: 172.30.1.1:5000/openshift/mysql@sha256:7da4a89bbef4d48c4b2286f61c37575acb45452aaec6986ee0b257cbb0aad3f5
            imagePullPolicy: IfNotPresent
            livenessProbe:
              failureThreshold: 3
              initialDelaySeconds: 30
              periodSeconds: 10
              successThreshold: 1
              tcpSocket:
                port: 3306
              timeoutSeconds: 1
            name: mysql
            ports:
            - containerPort: 3306
              protocol: TCP
            readinessProbe:
              exec:
                command:
                - /bin/sh
                - -i
                - -c
                - MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u $MYSQL_USER -D $MYSQL_DATABASE
                  -e 'SELECT 1'
              failureThreshold: 3
              initialDelaySeconds: 5
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 1
            resources:
              limits:
                memory: 512Mi
            volumeMounts:
            - mountPath: /var/lib/mysql/data
              name: mysql-data
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          schedulerName: default-scheduler
          securityContext: {}
          terminationGracePeriodSeconds: 30
          volumes:
          - name: mysql-data
            type: EmptyDir
      test: false
      triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
          - mysql
          from:
            kind: ImageStreamTag
            name: mysql:5.7
            namespace: openshift
        type: ImageChange
      - type: ConfigChange
    status:
      availableReplicas: 0
      latestVersion: 0
      observedGeneration: 0
      replicas: 0
      unavailableReplicas: 0
      updatedReplicas: 0
  - kind: "Service"
    apiVersion: "v1"
    metadata:
      annotations:
        template.openshift.io/expose-uri: mysql://{.spec.clusterIP}:{.spec.ports[?(.name=="mysql")].port}
      creationTimestamp: null
      labels:
        app: mysql
        template: mysql-template
      name: mysql
    spec:
      ports:
      - name: mysql
        port: 3306
        protocol: TCP
        targetPort: 3306
      selector:
        name: mysql
      sessionAffinity: None
      type: ClusterIP
    status:
      loadBalancer: {}
```

5. Następnie sprawdzamy czy pod uruchomił się poprawnie `oc get pods`

6. Wystaw port używając komendy: `oc port-forward _nazwaPoda_ 3306:3306`

7. Nie **przerywając** wcześniejszej komendy, otwórz **nowe** okno terminalu i zweryfikuj działanie poda

   > Pamiętaj, że trzeba mieć zainstalowany mysql na systemie

   `mysql -uroot -pdo280-admin -h127.0.0.1` - w ten sposób otrzymaliśmy dostęp do pod-a

   Również pojawiły się nasze wprowadzone secrety:

   ```
   mysql> show databases;
   +--------------------+
   | Database           |
   +--------------------+
   | information_schema |
   | mysql              |
   | performance_schema |
   | sys                |
   | testmysql          |
   +--------------------+
   5 rows in set (0.01 sec)
   ```



### 5.8 Managing Security Policies

Openshift definiuje właściwie dwie grupy w których użytkownik może coś wykonać: _project-related_ i _administration-related_.

Wyróżnamy też role:

* cluster-admin - wszystcy użytkownicy w tej grupie mogą zarządzać klastrem openshiftowym
* cluster-status - tylko tryb do odczytu co znajduje się w klastrze

np. `oc adm policy add-cluster-role-to-user cluster-admin username` 

Usuwanie odbywa się analogicznie. 

Bardzo przydatną funkcją jest natomiast:

`oc adm policy who-can delete user` 

Przykładowy output:

```
[root@Mateusz-Komputer mawocka]# oc adm policy who-can modify deploymentconfig
Namespace: default
Verb:      modify
Resource:  deploymentconfigs.apps.openshift.io

Users:  system:admin
        system:serviceaccount:default:pvinstaller
        system:serviceaccount:kube-system:clusterrole-aggregation-controller
        system:serviceaccount:openshift-core-operators:openshift-web-console-operator

Groups: system:cluster-admins
        system:masters
```



**Lokalne polityki**

| Role             | Opis                                                         |
| ---------------- | ------------------------------------------------------------ |
| edit             | Użytkownicy mogą tworzyć, zmieniać i usuwać większość zasobów aplikacyjnych z projektu jak serwisy czy dc. Nie mogą oni zmianiać natomiast limitów, quotas oraz zarządzać dostępami do projektu. |
| basic-user       | Dostęp read-only do projektu.                                |
| self-provisioner | Użytkownicy mogą tworzyć nowe projekty, jest to rola klastra a nie projektu |
| admin            | Użytkownicy w tej grupie mogą zarządzać wszystkimi zasobami oraz zwiększać uprawnienia w projektach wszystkich użytkowników. |

Jeśli chcemy nadać uprawnienia dla danej osoby w danym projekcie należy:

`oc adm policy add-role-to-user role-name username -n project` 



### 5.9 SCC - II

SCC są to restrykcje dotyczące konkretnych zasobów. Aby wyświetlić zdefiniowane polityki należy wykonać komendę`oc get scc` OpenShift: **anyuid**, **hostaccess**, **hostmount-anyuid**, **nonroot**, **privileged**, **restricted**. Możemy użyć komendy `oc describe scc anyuid` żeby otrzymać więcej informacji na dany temat.

Możemy również dodać serviceaccount jako root-a do naszego SCC poprzez:

`oc adm policy add-scc-to-user SCC -z service-account` 

Możemy edytować również cokolwiek.

`oc export scc restricted > custom_selinux.yml`

np. pozwolić na to żeby każdy user mógł być uruchomiony nawet root.

W miejscu seLinuxContext zmienić typ z MustRunAs na **RunAsAny**.

I następnie `oc create -f custom_selinux.yml`.



### 5.10 Duże zadanie

1. Stwórz użytkownika **user-review** 

   `oc create user user-review`

2. Usuń opcje **self-provisioner** dla regularnych użytkowników

   `oc adm policy remove-cluster-role-from-group self-provisioner system:authenticated system:authenticated:oauth`

3. Sprawdź czy ta opcja działa - zaloguj się na konto usera `oc login -u user-review` i wpisz `oc new-project test` 

   Otrzymany błąd powinien być następujący: `Error from server (Forbidden): You may not request a new project via this API.`

4. Zaloguj się na konto administratora i stwórz nowy projekt: `oc new-project secure-review`

5. Połącz konto **user-review** z projektem na prawach dewelopera: `oc adm policy add-role-to-user edit user-review -n secure-review`

6. Przygotuj plik yaml z poprzednim serwisem msql

7. Utwórz secrety jak powyżej

   ```
   oc create secret generic mysql \
    --from-literal='database-user'='mysql' \
    --from-literal='database-password'='redhat' \
    --from-literal='database-name'='testmysql' \
    --from-literal='database-root-password'='do280-admin'
   ```

8. Zrób deploy aplikacji z pliku: `oc new-app --file=mysql-lab.yaml `

9. Stwórz przekierowanie portu i sprawdź czy potrafisz dostać się do aplikacji

   `oc port-forward mysql-1-9mmk8 3306:3306`

   `mysql -uroot -pdo280-admin -h127.0.0.1`

10. Stwórz nową aplikację phpmyadmin z dockerhub-a

    `oc new-app --docker-image=phpmyadmin/phpmyadmin -e PMA_HOST=mysql.test.svc`

    Aplikacja ta niestety się nie uruchomi, aby mogła się uruchomić należy zmienić polityki SCC

11. Logujemy się jako admin `oc login -u system:admin` i tworzymy konto serwisowe `oc create serviceaccount phpmyadmin-account` łączymy to konto z **anyuid**: `oc adm policy add-scc-to-user anyuid -z phpmyadmin-account`. Następnie edytujemy dc config: 

    ```
    oc patch dc/phpmyadmin --patch='{"spec":{"template":{"spec":{"serviceAccountName": "phpmyadmin-account"}}}}'
    ```

12. Stwórz nowego routa do aplikacji

    `oc expose svc/phpmyadmin --hostname=phpmyadmin-secure-review.192.168.42.157.nip.io`



## 6. Persistent Storage



Domyślną opcją w OpenShifcie jest **nie** montowany system plików w podach. To znaczy że pliki są efemeryczne, czyli znikają podczas restartu poda. OpenShift natomiast umożliwia dodanie takiej opcji jako **PV** - persistant volume. PV mogą być montowane i wspierają różne typy łączenia dysków jak: NFS czy GlustesFS. Również dostęp do PV jest możliwy do ustawienia. **ReadWriteOnce** (RWO) - wolumin może zostać podmontowany i używany do zapisu i oczytu przez jednego noda, **ReadOnlyMany** (ROX) - wolumin podmontowany ma prawa tylko do odczytu przez wiele nodów, **ReadWriteMany** (RWX) - wolumin ma prawa read/write dla wielu nodów. 

**Konfiguracje NFS-a znajdziemy na 172str w DO280** i [tu](https://docs.okd.io/latest/install_config/persistent_storage/persistent_storage_nfs.html).

Aby sprawdzić podmontowane NFS-y: `showmount -e`

Uruchamiamy nfs-a na _minishifcie_:

1. Otwórz konsole minishifta: `minishift ssh` zaloguj się jako root `sudo su`
2. Wykonaj komendę: `service nfs start` i sprawdź czy nfs uruchomił się prawidłowo `service nfs status`
3. Sprawdź swój hostname `env | grep HOSTNAME`
4. Ustawiamy NFS-a

Musi mieć:

* Ustawiony user i grupe jako **nfsnobody** `chown nfsnobody:nfsnobody /katalog`
* Ustawiony dostęp na **0700** `chmod 0700 /katalog`
* Wyeksportowaną opcję **all_squash** (o tym za chwilę)

Ok więc tworzymy folder na **minishifcie** `mkdir -p /data/nfs/1` i stosujemy dwa z pierwszych wymogów. 

Wynik jest tutaj:

```
[root@minishift /]# chown nfsnobody:nfsnobody /data/nfs/1/
[root@minishift /]# chmod 0700 /data/nfs/1/
[root@minishift /]# ls -la /data/nfs/
total 12
drwxr-xr-x. 3 root      root      4096 Sep  6 10:54 .
drwxr-xr-x. 3 root      root      4096 Sep  6 10:54 ..
drwx------. 2 nfsnobody nfsnobody 4096 Sep  6 10:54 1
```

Teraz trzeci punkt. Edytuj plik `vi /etc/exports` i wprowadź linie: `/data/nfs/1/ *(rw,async,all_squash)` po czym zrestartuj nfs-a `systemctl restart nfs-server` sprawdź status `systemctl status nfs-server` i zobacz podmontowane katalogi: `showmount -e`

```
[root@minishift /]# showmount -e
Export list for minishift:
/data/nfs/1 *
```

Od tego momentu NFS jest gotowy do użycia. Więc stwórzmy aplikajcę która z tego korzysta.



### 6.1 Krótkie zadanie

1. Wykonaj wcześniejsze punkty, aby połączyć NFS-a

2. Zaloguj się jako **system:admin** i stwórz PV komendą `oc create -f pv.yml`

   ```yaml
   apiVersion: v1
   kind: PersistentVolume
   metadata:
     name: mysqldb-volume
   spec:
     capacity:
       storage: 1Gi
     accessModes:
       - ReadWriteMany
     nfs:
       path: /data/nfs/1
       server: minishift
   ```

3. Potwierdź stworzonego wolumena używając `oc get pv` następnie zaloguj się jako **developer**

4. Stwórz nowy projekt `oc new-project persistant-storage`

5. I jak poprzednio stwórz secrety oraz zrób deploy aplikacji z pliku (jak w przykładzie 5.7)

6. Dodaj claima:

   ```
   oc set volume dc/mysql --add --overwrite --name=mysql-data -t pvc --claim-name=mysql-pvclaim --claim-size=1Gi --claim-mode='ReadWriteMany'
   ```

7. Przekieruj port komedą `oc port-forward mysql-2-l8b4s 3306:3306`

   I dodaj do bazy danych: `CREATE TABLE pet (name VARCHAR(20), owner VARCHAR(20), species VARCHAR(20), sex CHAR(1), birth DATE, death DATE);`

8. Zweryfikuj czy na NFS-ie pojawiły się jakieś pliki `minishift ssh` i `ls -la /data/nfs/1/mysql`

9. Usuń projekt `oc delete project persistant-storage`



## 7. Managing application deployments



### 7.1. Skalowanie



_Replication Controllers_  gwarantuje nam, że tyle podów ile uruchomiliśmy tyle będzie działać przez cały czas.

Możemy zdefiniować w _dc_ ile razy dany _pod_ zostanie zreplikowany. Do tego służy dyrektywa **"replicas"** używana tak jak w przykładzie poniżej.

```yaml
apiVersion: v1
kind: List
items:
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      creationTimestamp: null
      generation: 1
      name: mysql
    spec:
      replicas: 1
      ...
```

Stwórz jakąkolwiek aplikację i przeskaluj ją:

* Pobierz informację o dc: `oc get dc`
* Wykonaj skalowanie: `oc scale --replicas=5 dc myapp`

Co się stanie? Stworzy się dokładnie 5 replik aplikacji, jeśli było wcześniej więcej wtedy repliki się usuną tak, że zostanie 5. 

Dużo lepszym sposobem jest skalowanie aplikacji zgodnie z pewnymi restrykcjami np. dotyczącymi ograniczenia zasobów. Jest to tak zwane **autoskalowanie** tworzy się wtedy coś co nazywa się **HorizontalPodAutoscaler** w skrócie **HPA**. Znowu mamy tutaj do czynienia z zasobem, tak jak było to kiedyś powiedziane, w Openshifcie wszystko jest zasobem. 

* Ustaw automatyczne skalowanie `oc autoscale dc/myapp --min 1 --max 10 --cpu-percent=80`
* Sprawdź to! `oc get hpa/myapp` oraz `oc describe hpa/myapp`
* **Do używania automatycznych skalowań niezbędne jest usawienie metryk! (o tym później)**

### 7.2. Controlling Pod Scheduling

Pody mogą zostać rozlokowane na różnych nodach, aby mieć większą kontrolę nad nimi stosuje się coś takiego jak **zones** i **regions**. Algorytm działania przedstawiony jest na 215 stronie DO280.

Przykładowe zastosowanie to:

* Nadanie nodom odpowiednich labeli: `oc label node localhost region=pl zone=wroclaw --overwrite`
* Sprawdzenie: `oc get node localhost --show-labels` a żeby wyspecifikować dokładniej można: `oc get node localhost -L region -L zone`

Kolejnym zagadnieniem jest wyłączenie możliwości deploymentu podów na dany node. Do tego możemy użyć polecenia: `oc adm manage-node --schedulable=false localhost`. Możemy też "wysuszyć" noda komendą `oc adm drain localhost`. Po zakończeniu prac serwisowych na danym nodzie możemy z powrotem go uruchomić: `oc adm manage-node --schedulable=true localhost`. 



Żeby pod uruchomił się na konkretnym nodzie o np. env = qa należy do jego dc dodać pewną deryktywę. `oc patch dc myapp --patch '{"spec":{"template":{"nodeSelector":{"env":"qa"}}}}'`.

W dużych projektach niektóre applikacje lub zasoby wysyła się na region infra. Do tego służy polecenie: `oc annotate --overwrite namespace default openshft.io/node-selector='region=infra'` 



### 7.3. Managing Images, Image Streams and Templates

W terminologi Openshifta obraz to coś co może zostać wdrożone jako pojedynczy kontener. Taki _ImageStream_ został pokazany na 225 stronie DO280. Taki plik można następnie otagować: `oc tag source destination` lub nadać mu inny tag niż posiada.



Aby wyciągnąć template z danej aplikacji należy wykonać: `oc export template nazwa > nazwa.yml` lub użyć opcji: `oc export dc application --as-template=app-template`. 



**Podsumowując** najważniejsze w tym rozdziale było tylko skalowanie i suszenie nodów w odpowiedni sposób.



## 8. Metryki

### 8.1 Jak zainstalować metryki na openshifcie 

* Sprawdź czy są dostępne: **metrics-casandra**, **metrics-hawkular-metrics**, **metrics-heapster**. Sprawdź czy folder, który będzie dołączony jako PV ma odpowiednie ustawienia.
* Stwórz odpowiednio folder jako NFS. Do tego posłużą skrypty jak wcześniej.
* Uruchom playbooka z opcjami **-e** 
* Wyciągnij routy: `oc get route -n openshift-infra` 
* Otwórz strone **hawkular** i zaakceptuj SSL-a



## 9. Managing and  monitoring 



### 9.1 Limiting Resource Usage

Czyli tak zwane **quotas**, mogą być one nadane na wartość kubernetesa czyli pody, serwisy, dc itp. lub także na cpu, memory itd. Są to restrykcje, które pomagają w utrzymaniu działania aplikacji, jeśli by ich nie było to np. baza danych zeżre całą pamięć fizyczną, a tak ewentualnie się wywali nie robiąc szkód. Bez **quota** zasoby mogą zostać wciągnięte całkowicie przez jakąkolwiek aplikację. 

Limity dla CPU podaje się w **m** natomiast dla memory w **Gi**. 

Stwórzmy sobie aplikację:

* `oc new-project quota-project`

* Tworzymy ostatnio uruchomianą bazę danych

* Tworzymy quota

  ```bash
  oc create quota mysql-lab \
  --hard=services=5 \
  --hard=cpu=1300m \
  --hard=memory=1.5Gi
  ```

* Sprawdź: `oc describe quota`

* Usuń: `oc delete quota mysql-lab`

Można również zdefiniować inne limity tak zwane **LimitRange** poniżej znajduje się tabela w które znajdują się dostępne opcje.

| Type      | Resource Name | Description                                                  |
| --------- | ------------- | ------------------------------------------------------------ |
| Container | cpu           | Min i Max CPU per container                                  |
| Container | memory        | Min i Max memory per container                               |
| Pod       | cpu           | Analogicznie                                                 |
| Pod       | memory        | Analogicznie                                                 |
| Image     | storage       | Maksymalny rozmiar obrazu, który może zostać wypchany        |
| PVC       | storage       | Min i Max dostępności, który może być poproszony przez jednego claima |

Przykładowy yaml:

```yaml
apiVersion: "v1"
kind: "LimitRange"
metadata:
  name: "mysql-limits"
spec:
  limits:
    - type: "Pod"
      max:
        cpu: "2"
        memory: "1Gi"
      min:
        cpu: "200m"
        memory: "200Mi"
```



Aplikujemy limity: `oc create -f mysql-limits.yaml` i sprawdzamy `oc describe limitranges mysql-limits`. To są limity na pody w projekcie. To znaczy że nie przekroczy tego pod nawet jeśli zdefiniujemy go inaczej w naszym templacie.

W przykładowej aplikacji znajduje już się limit: `grep -A 1 'limits' mysql-lab.yaml`

```
[root@Mateusz-Komputer ~]# grep -A 1 'limits' mysql-lab.yaml 
              limits:
                memory: 512Mi
```

Podnieśmy ten limit do 2Gi i zobaczmy co się stanie. 

* `oc export dc/mysql > export.yaml`
* Zmodyfikuj wartość
* `oc apply -f export.yaml`

Werdykt: Pod nie wstanie.



### 9.2. Monitoring aplikacji za pomocą probes

Są trzy typy sprawdzenia czy pod jest zdrowy:

```
readinessProbe:
  httpGet:
    path: /health
    port: 8080
  initialDelaySecounds: 15
  timeoutSecounds: 1
```



```
livenessProbe:
  exec:
    command:
      - cat 
      - /tmp/healt
  initialDelaySecounds: 15
  timeoutSecounds: 1
```



```
livenessProbe:
  tcpSocket:
    port: 8080
  initialDelaySecounds: 15
  timeoutSecounds: 1
```

 

Można dodać to za pomocą UI jak i w definicji samego pod-a w templatce.

Najważniejsza różnica pomiędzy readinessProbe a livenessProbe to, że livenessProbe sprawdzamy czy kontener dalej działa a w drugim przypadku sprawszamy czy dana funkcjonalość wciąż jest dostępna.

